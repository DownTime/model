﻿import { Component, OnInit } from '@angular/core';

import { ModalService } from '../_services';


@Component({ templateUrl: 'test-page.component.html' })
export class TestPageComponent implements OnInit {
    private bodyText: string;

    constructor(private modalService: ModalService) {
    }

    ngOnInit() {
        this.bodyText = 'This one doesn\'t have any modals...';
    }

    openModal(id: string) {
        this.modalService.open(id);
    }

    closeModal(id: string) {
        this.modalService.close(id);
    }
}